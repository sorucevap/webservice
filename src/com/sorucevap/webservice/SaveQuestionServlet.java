package com.sorucevap.webservice;

import javax.ejb.EJBException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.sorucevap.srv.SoruCevapService;
import com.sorucevap.srv.entity.Question;
import com.sorucevap.webservice.util.BaseHttpServlet;

/**
 * Servlet implementation class Authenticate
 */
@WebServlet( "/savequestion" )
public class SaveQuestionServlet extends BaseHttpServlet
{
	private static final long serialVersionUID = 1L;
	private static SoruCevapService service = SoruCevapService.Locator.getService();
	private static Logger logger = Logger.getLogger( SaveQuestionServlet.class );
	
	@Override
	protected void processRequest( HttpServletRequest request)
	{
		try {
			String token = request.getParameter( "token" );
			int idcategory = Integer.parseInt( request.getParameter( "idcategory" ) );
			String question = request.getParameter( "question" );
			
			Question ques = service.saveQuestion( token, idcategory, question );
			if ( ques == null ) {
				printError( "Geçersiz parametreler!" );
			} else {
				printResult( "OK" );
			}
		} catch ( EJBException e ) {
			logger.warn( "EJBEXCEPTION : " + e.getCause().getMessage() );
			printError( e.getCause().getMessage() );
		} catch ( Exception e ) {
			logger.error( " EXCEPTION : ", e );
			printError( e.getMessage() );
		}
	}
}
