package com.sorucevap.webservice.util;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class GWTJsonServlet extends BaseHttpServlet
{
	protected Hashtable<Long, String> callbackht = new Hashtable<Long, String>();
	protected Hashtable<Long, String> failureht = new Hashtable<Long, String>();
	
	public GWTJsonServlet()
	{
	}
	
	@Override
	protected void doGet( HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		doPost( req, resp );
	}
	
	@Override
	protected void doPost( HttpServletRequest request, HttpServletResponse response) throws ServletException,
	        IOException
	{
		ipht.put( Thread.currentThread().getId(), request.getRemoteAddr() );
		request.setCharacterEncoding( "UTF-8" );
		response.setCharacterEncoding( "UTF-8" );
		response.setContentType( "application/json" );
		
		poutht.put( Thread.currentThread().getId(), response );
		callbackht.put( Thread.currentThread().getId(), request.getParameter( "callback" ) + "" );
		failureht.put( Thread.currentThread().getId(), request.getParameter( "failure" ) + "" );
		processRequest( request );
	}
	
	public void printResult( String result)
	{
		if ( callback().equals( "null" ) ) {
			super.printResult( result );
		} else {
			super.printResult( callback() + "(" + result + ")" );
		}
		finalizeRequest();
	}
	
	public void printError( String error)
	{
		if ( failure().equals( "null" ) ) {
			super.printError( error );
		} else {
			super.printResult( failure() + "(" + error + ")" );
		}
		finalizeRequest();
	}
	
	protected String callback()
	{
		return callbackht.get( Thread.currentThread().getId() );
	}
	
	protected String failure()
	{
		return failureht.get( Thread.currentThread().getId() );
	}
	
	private void finalizeRequest()
	{
		callbackht.remove( Thread.currentThread().getId() );
		failureht.remove( Thread.currentThread().getId() );
	}
}
