package com.sorucevap.webservice.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public abstract class BaseHttpServlet extends HttpServlet
{
	protected Hashtable<Long, HttpServletResponse> poutht = new Hashtable<Long, HttpServletResponse>();
	protected Hashtable<Long, String> ipht = new Hashtable<Long, String>();
	
	private static Logger logger = Logger.getLogger( BaseHttpServlet.class );
	
	protected abstract void processRequest( HttpServletRequest request);
	
	@Override
	protected void doGet( HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		doPost( req, resp );
	}
	
	@Override
	protected void doPost( HttpServletRequest request, HttpServletResponse response) throws ServletException,
	        IOException
	{
		ipht.put( Thread.currentThread().getId(), request.getRemoteAddr() );
		request.setCharacterEncoding( "UTF-8" );
		response.setCharacterEncoding( "UTF-8" );
		response.setContentType( "text/html" );
		
		poutht.put( Thread.currentThread().getId(), response );
		processRequest( request );
	}
	
	public void printResult( String result)
	{
		out().print( result );
		finalizeRequest();
	}
	
	public void printError( String error)
	{
		out().print( "Exception: " + error );
		finalizeRequest();
	}
	
	public void logHacker( String clazz)
	{
		Logger logger = Logger.getLogger( clazz );
		logger.error( "*************** HACKER ATTEMPT *********************" );
		logger.error( "IP: " + getRequestIP() );
		logger.error( "Method: " + clazz );
		logger.error( "*************** HACKER ATTEMPT *********************" );
	}
	
	protected HttpServletResponse response()
	{
		return poutht.get( Thread.currentThread().getId() );
	}
	
	protected PrintWriter out()
	{
		try {
			return response().getWriter();
		} catch ( IOException e ) {
			e.printStackTrace();
			throw new RuntimeException( "Response writer didn't find!" );
		}
	}
	
	protected String getRequestIP()
	{
		return ipht.get( Thread.currentThread().getId() );
	}
	
	private void finalizeRequest()
	{
		try {
			out().flush();
			out().close();
		} catch ( Exception e ) {
			logger.error( "BaseHttpServlet finalizeRequest Method Failed IP: " + getRequestIP(), e );
		} finally {
			poutht.remove( Thread.currentThread().getId() );
			ipht.remove( Thread.currentThread().getId() );
		}
	}
	
	public void redirect( String host)
	{
		response().setStatus( HttpServletResponse.SC_TEMPORARY_REDIRECT );
		response().setHeader( "Location", host );
	}
	
	protected String getEntity( HttpServletRequest req)
	{
		int conlen = req.getContentLength();
		if ( conlen <= 0 ) throw new RuntimeException( "Request Content Length Invalid!" );
		StringWriter sw = new StringWriter();
		char[] bulk = new char[1024];
		try {
			BufferedReader isr = new BufferedReader( new InputStreamReader( req.getInputStream(), "UTF-8" ) );
			while ( conlen >= 0 ) {
				int read = isr.read( bulk );
				if ( read > 0 )
					sw.write( bulk, 0, read );
				else
					break;
				conlen -= read;
			}
		} catch ( Exception ex ) {
			ex.printStackTrace();
		}
		return sw.toString();
	}
}
