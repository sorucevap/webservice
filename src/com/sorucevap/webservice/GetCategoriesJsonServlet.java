package com.sorucevap.webservice;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJBException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.sorucevap.srv.SoruCevapService;
import com.sorucevap.srv.entity.Category;
import com.sorucevap.webservice.util.CategoryVO;
import com.sorucevap.webservice.util.GWTJsonServlet;
import com.sorucevap.webservice.util.Jsonutil;

/**
 * Servlet implementation class Authenticate
 */
@WebServlet( "/getcategories" )
public class GetCategoriesJsonServlet extends GWTJsonServlet
{
	private static final long serialVersionUID = 1L;
	private static SoruCevapService service = SoruCevapService.Locator.getService();
	private static Logger logger = Logger.getLogger( GetCategoriesJsonServlet.class );
	
	@Override
	protected void processRequest( HttpServletRequest request)
	{
		try {
			List<Category> list = service.getCategories();
			List<CategoryVO> result = new ArrayList<CategoryVO>();
			for ( Category c : list ) {
				result.add( new CategoryVO( c.getIdcategory(), c.getName() ) );
			}
			printResult( Jsonutil.toJsonList( result ) );
		} catch ( EJBException e ) {
			logger.warn( "EJBEXCEPTION : " + e.getCause().getMessage() );
			printError( Jsonutil.toJsonObj( e.getCause().getMessage() ) );
		} catch ( Exception e ) {
			logger.error( "EXCEPTION : ", e );
			printError( Jsonutil.toJsonObj( e.getMessage() ) );
		}
	}
}
