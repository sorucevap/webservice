package com.sorucevap.webservice;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJBException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.sorucevap.srv.SoruCevapService;
import com.sorucevap.srv.entity.Question;
import com.sorucevap.webservice.util.GWTJsonServlet;
import com.sorucevap.webservice.util.Jsonutil;
import com.sorucevap.webservice.util.QuestionVO;

/**
 * Servlet implementation class Authenticate
 */
@WebServlet( "/getquestions" )
public class GetQuestionsJsonServlet extends GWTJsonServlet
{
	private static final long serialVersionUID = 1L;
	private static SoruCevapService service = SoruCevapService.Locator.getService();
	private static Logger logger = Logger.getLogger( GetQuestionsJsonServlet.class );
	
	@Override
	protected void processRequest( HttpServletRequest request)
	{
		try {
			int idcategory = Integer.parseInt( request.getParameter( "idcategory" ) );
			
			List<Question> list = service.getQuestions( idcategory );
			List<QuestionVO> result = new ArrayList<QuestionVO>();
			for ( Question q : list ) {
				result.add( new QuestionVO( q.getIdquestion(), q.getUser().getEmail(), q.getCategory().getName(), q
				        .getTitle(), q.getDescription(), q.getDate() ) );
			}
			printResult( Jsonutil.toJsonList( result ) );
		} catch ( EJBException e ) {
			logger.warn( "EJBEXCEPTION : " + e.getCause().getMessage() );
			printError( Jsonutil.toJsonObj( e.getCause().getMessage() ) );
		} catch ( Exception e ) {
			logger.error( "EXCEPTION : ", e );
			printError( Jsonutil.toJsonObj( e.getMessage() ) );
		}
	}
}
