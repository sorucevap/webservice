package com.sorucevap.webservice;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJBException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.sorucevap.srv.SoruCevapService;
import com.sorucevap.srv.entity.Answer;
import com.sorucevap.webservice.util.AnswerVO;
import com.sorucevap.webservice.util.GWTJsonServlet;
import com.sorucevap.webservice.util.Jsonutil;

/**
 * Servlet implementation class Authenticate
 */
@WebServlet( "/getanswers" )
public class GetAnswersJsonServlet extends GWTJsonServlet
{
	private static final long serialVersionUID = 1L;
	private static SoruCevapService service = SoruCevapService.Locator.getService();
	private static Logger logger = Logger.getLogger( GetAnswersJsonServlet.class );
	
	@Override
	protected void processRequest( HttpServletRequest request)
	{
		try {
			int idquestion = Integer.parseInt( request.getParameter( "idquestion" ) );
			
			List<Answer> list = service.getAnswers( idquestion );
			List<AnswerVO> result = new ArrayList<AnswerVO>();
			for ( Answer a : list ) {
				result.add( new AnswerVO( a.getIdanswer(), a.getUser().getEmail(), a.getQuestion().getIdquestion(), a
				        .getDescription(), a.getDate() ) );
			}
			printResult( Jsonutil.toJsonList( result ) );
		} catch ( EJBException e ) {
			logger.warn( "EJBEXCEPTION : " + e.getCause().getMessage() );
			printError( Jsonutil.toJsonObj( e.getCause().getMessage() ) );
		} catch ( Exception e ) {
			logger.error( "EXCEPTION : ", e );
			printError( Jsonutil.toJsonObj( e.getMessage() ) );
		}
	}
}
