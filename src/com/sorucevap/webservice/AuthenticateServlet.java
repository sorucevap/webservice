package com.sorucevap.webservice;

import javax.ejb.EJBException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.sorucevap.srv.SoruCevapService;
import com.sorucevap.srv.entity.User;
import com.sorucevap.webservice.util.BaseHttpServlet;

/**
 * Servlet implementation class Authenticate
 */
@WebServlet( "/auth" )
public class AuthenticateServlet extends BaseHttpServlet
{
	private static final long serialVersionUID = 1L;
	private static SoruCevapService service = SoruCevapService.Locator.getService();
	private static Logger logger = Logger.getLogger( AuthenticateServlet.class );
	
	@Override
	protected void processRequest( HttpServletRequest request)
	{
		try {
			String email = request.getParameter( "email" );
			String pwd = request.getParameter( "pwd" );
			int idgroup = Integer.parseInt( request.getParameter( "idgroup" ) );
			String token = request.getSession().getId();
			
			User user = service.authenticate( email, pwd, idgroup, token );
			if ( user == null ) {
				printError( "Geçersiz Kullanıcı/Parola " + email + "@" + pwd );
			} else {
				printResult( token );
			}
		} catch ( EJBException e ) {
			logger.warn( "EJBEXCEPTION : " + e.getCause().getMessage() );
			printError( e.getCause().getMessage() );
		} catch ( Exception e ) {
			logger.error( " EXCEPTION : ", e );
			printError( e.getMessage() );
		}
	}
}
