package com.sorucevap.webservice;

import javax.ejb.EJBException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.sorucevap.srv.SoruCevapService;
import com.sorucevap.srv.entity.Category;
import com.sorucevap.webservice.util.BaseHttpServlet;

/**
 * Servlet implementation class Authenticate
 */
@WebServlet( "/savecategory" )
public class SaveCategoryServlet extends BaseHttpServlet
{
	private static final long serialVersionUID = 1L;
	private static SoruCevapService service = SoruCevapService.Locator.getService();
	private static Logger logger = Logger.getLogger( SaveCategoryServlet.class );
	
	@Override
	protected void processRequest( HttpServletRequest request)
	{
		try {
			String token = request.getParameter( "token" );
			String category = request.getParameter( "name" );
			
			Category cat = service.saveCategory( token, category );
			if ( cat == null ) {
				printError( "Geçersiz parametreler!" );
			} else {
				printResult( "OK" );
			}
		} catch ( EJBException e ) {
			logger.warn( "EJBEXCEPTION : " + e.getCause().getMessage() );
			printError( e.getCause().getMessage() );
		} catch ( Exception e ) {
			logger.error( " EXCEPTION : ", e );
			printError( e.getMessage() );
		}
	}
}
